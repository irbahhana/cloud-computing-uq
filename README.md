# Inacademi
Online psychology assessment, built with react and django.
## running the application
Clone the repository
```shell script
git clone https://gitlab.com/irbahhana/cloud-computing-uq.git
```

change directory to pulled directory
```shell script
cd cloud-computing-uq
```

run docker-compose buld
```shell script
docker-compose build
```

run docker-compose up
```shell script
docker-compose up
```
## Troubleshoot
1. if you encountered error in port configuration, check port number 80, 8000, or 5432 to ensure no service are running in that port use the command:
```
sudo lsof -i:{your port}
```
