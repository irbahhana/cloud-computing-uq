import React, {useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Navbar from "../components/navbar";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {isDev} from "../utils/config";
import Cfit from "../result/cfit";
import Spm from "../result/spm";
import Riasec from "../result/riasec";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100vw',
        height: '100vh',
        overflowY: 'scroll',
        paddingLeft: '120px',
        paddingRight: '120px',
        background: 'linear-gradient(107.56deg, #8357FF 0%, #612CD3 40.1%, #751DCB 100%);',
    },
    padding: {
        paddingTop: '5rem'
    },
    paddingOne: {
        padding: '14px'
    },
    content: {
        display: 'flex',
        flexDirection:'column'
    },
    searchUser: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    result: {
        display: 'flex',
        paddingTop:'32px',
        flexDirection: 'column',
    },
    inputBox: {
        background: '#FFFFFF',
        width: '300px'
    },
    tai: {
        display: 'flex',
    },
    title: {
        color: '#FFFFFF'
    },
    paperMargin: {
        marginLeft: '1rem',
        marginTop: '1rem',
        marginRight: '1rem',
        marginBottom: '1rem'
    },
    searchUserContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',

    },
    submit: {
        color: "white",
        '&.MuiButton-outlinedSecondary': {
            border: '1px solid white',
        },
        borderRadius: '8px',
        width: '131px'
    },
    input: {
        display: 'flex',
        flexDirection: 'column'
    },
    formWrapper: {
        display: 'flex',
        flexDirection: 'row',
    },
    iqTest:{
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-evenly",
        flex:"1"
    }
}));


function Result({code, setCode}) {
    const classes = useStyles();
    const axios = require('axios')
    const fileDownload = require('js-file-download');
    const [isLogin, setIsLogin] = useState(false)
    const [riasec, setRiasec] = useState({
        result: '',
        r: 0,
        i: 0,
        a: 0,
        s: 0,
        e: 0,
        c: 0
    })
    const [spm, setSpm] = useState({
        "result": null,
        "iq": null
    })
    const [cfit, setCfit] = useState({
        "score": null,
        "iq": null
    })
    const [user, setUser] = useState({
        code: '',
        name: '',

    })

    function downloadResult(event) {
        event.preventDefault()
        const url = isDev() ? `http://127.0.0.1:8000/api/result/` : `/api/result/`
        axios.post(
            url,
            {
                "user": user,
                "riasec": riasec,
                "spm": spm,
                "cfit": cfit
            }
        ).then(r => {
            // fileDownload(r.data, code + "_" + user.name + '.pdf')
            console.log(r.data)
        })
    }

    function handleChange(event) {
        setCode(event.target.value)
    }

    function get_result() {
        const url = isDev() ? `http://127.0.0.1:8000/api/result/${code}/` : `/api/result/${code}/`
        axios.get(url)
            .then((response) => {
                setRiasec(response.data['riasec'] ? response.data['riasec'] : null)
                setSpm(response.data['spm'] ? response.data['spm'] : null)
                setCfit(response.data['cfit'] ? response.data['cfit'] : null)
            })
    }

    function handleSubmitCode(event) {
        const url = isDev() ? `http://localhost:8000/api/view/${code}/` : `/api/view/${code}/`
        event.preventDefault()
        axios.get(url)
            .then((response) => {
                if (response.data) {
                    setUser(response.data)
                    setIsLogin(true)
                    get_result()
                } else {
                    alert("Code not found")
                }
            })
    }

    if (isLogin) {
        return (
            <div className={classes.root}>
                <Navbar/>
                <div className={classes.padding}/>
                <div className={classes.content}>
                    <div className={classes.searchUserContainer}>
                        <Typography variant={"h4"} style={{
                            fontWeight: "bold",
                            color: '#FFF',
                            display: "flex",
                            flexDirection: "flex-start"
                        }}>
                            Result Page
                        </Typography>
                        <div className={classes.paddingOne}/>
                        <form onSubmit={handleSubmitCode} className={classes.tai}>
                            <div className={classes.input}>
                                <Typography variant='h7' style={{textAlign:"left", color:"white"}}>
                                    <strong>
                                        Masukan kode anda
                                    </strong>
                                </Typography>
                                <div className={classes.formWrapper}>
                                    <TextField name="code"
                                               className={classes.inputBox}
                                               color="secondary"
                                               value={code}
                                               variant="filled"
                                               label="Ex: ABCD"
                                               onChange={handleChange}
                                               inputProps={{maxLength: 4}}
                                               required={true}
                                    />
                                    <div className={classes.paddingOne}/>
                                    <Button variant={"outlined"} color={'secondary'} className={classes.submit}
                                            type={"submit"}> Lihat Hasil</Button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className={classes.result}>
                        <div className={classes.iqTest}>
                            <Cfit cfit={cfit}/>
                            <Spm spm={spm}/>
                        </div>
                        <div className={classes.paddingOne}/>
                        <Riasec riasec={riasec}/>
                        {/*<form onSubmit={downloadResult}>*/}
                        {/*    {*/}
                        {/*        riasec && spm && cfit ?*/}
                        {/*            <Button variant={'contained'} type="submit">*/}
                        {/*                Unduh Hasil Test Anda*/}
                        {/*            </Button>*/}
                        {/*            :*/}
                        {/*            <div>*/}
                        {/*                <p> Hasil test hanya bisa diunduh ketika semua test telah dilakukan</p>*/}
                        {/*                <Button disabled>*/}
                        {/*                    Unduh Hasil Test Anda*/}
                        {/*                </Button>*/}
                        {/*            </div>*/}
                        {/*    }*/}
                        {/*</form>*/}


                        {/*<Riasec riasec={riasec}/>*/}
                    </div>
                    <div className={classes.padding}/>
                </div>
            </div>
        )
    } else {
        return (
            <div className={classes.root}>
                <Navbar/>
                <div className={classes.padding}/>
                <div className={classes.content}>
                    <div className={classes.searchUserContainer}>
                        <Typography variant={"h4"} style={{
                            fontWeight: "bold",
                            color: '#FFF',
                            display: "flex",
                            flexDirection: "flex-start"
                        }}>
                            Result Page
                        </Typography>
                        <div className={classes.paddingOne}/>

                        <form onSubmit={handleSubmitCode} className={classes.tai}>
                            <div className={classes.input}>
                                <Typography variant='h7' style={{textAlign:"left", color:"white"}}>
                                    <strong>
                                        Masukan kode anda
                                    </strong>
                                </Typography>
                                <div className={classes.formWrapper}>
                                    <TextField name="code"
                                               className={classes.inputBox}
                                               color="secondary"
                                               value={code}
                                               variant="filled"
                                               label="Ex: ABCD"
                                               onChange={handleChange}
                                               inputProps={{maxLength: 4}}
                                               required={true}
                                    />
                                    <div className={classes.paddingOne}/>
                                    <Button variant={"outlined"} color={'secondary'} className={classes.submit}
                                            type={"submit"}> Lihat
                                        Hasil</Button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Result;
