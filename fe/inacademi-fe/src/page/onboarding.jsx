import React, {useState} from 'react';
import Box from "@material-ui/core/Box";
import Navbar from "../components/navbar";
import Modal from "@material-ui/core/Modal";
import {Alert, AlertTitle} from "@material-ui/lab";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {useRecoilState} from "recoil";
import testAtom from "../atom/testAtom";
import {useHistory} from "react-router-dom";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {isDev} from "../utils/config";

const useStyles = makeStyles((theme) => ({
    boxStyles: {
        width: '100vw',
        height: '100vh',
        paddingTop: '25px',
        paddingLeft: '120px',
        paddingRight: '120px'
    },
    title: {
        color: '#FFFFFF',
        textAlign: 'justify'
    },
    padding: {
        margin: '1rem'
    },
    inputBox: {
        background: '#FFFFFF',
        borderRadius: '8px',
        width: '300px',
    },
    choice: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardRoot: {
        width: '316px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-evenly'
    },
    cardBody: {
        textAlign: "justify"
    },
    navbarPadding: {
        marginTop: '5rem',
        display: "flex",
        flexDirection: "column",
        justifyContent: 'flex-start'
    },
    paddingBottom: {
        padding: '3rem'
    },
    content: {
        color: "#FFFFFF",
        textAlign: 'left'
    },
    codeInput: {
        marginTop: '2rem',
        display: "flex",
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    code: {
        background: '#6417AF',
        width: '210px',
        height: '40px',
        borderRadius: '10px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
}));

const Onboarding = ({code,setCode}) => {
    const classes = useStyles()
    const [name, setName] = useState("")
    const [codeForm, setCodeForm] = useState('')
    const [testType, setTestType] = useRecoilState(testAtom)
    const history = useHistory()
    const [modalAlert, setModalAlert] = useState(false)

    function handleChange(event) {
        setName(event.target.value)
    }

    function handleSubmitCode() {
        const axios = require('axios').default;
        const url = isDev() ? `http://localhost:8000/api/tester/${code}/` : `/api/tester/${code}/`
        const form = new FormData()
        if (name === "") {
            handleOpen()
            return
        }
        form.append("name", name)
        axios.put(url, form).then(
            (response) => {
                console.log(response.data)
                setCode(response.data.code)
            }
        ).then(() => {
            window.localStorage.setItem("code", code)
            history.push('/assessment')
        })
    }

    const handleOpen = () => {
        setModalAlert(true)
    }




    return (
        <Box className={classes.boxStyles}>
            <Navbar/>
            {
                modalAlert ?
                    <Modal
                        open={modalAlert}
                        onClose={() => setModalAlert(false)}
                    >
                        <Alert severity="warning">
                            <AlertTitle>Name cannot be empty</AlertTitle>
                            Please fill the name before choosing the test
                        </Alert>
                    </Modal>
                    :
                    null
            }
            <div className={classes.navbarPadding}>
                <Typography variant="h4" className={classes.title}>
                    <strong>
                        Selamat Datang Di PsyMe!
                    </strong>
                </Typography>
                <p className={classes.content}>
                    Tes Psikologi bertujuan Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                </p>
            </div>
            <div className={classes.codeInput}>
                <TextField
                    required={true}
                    name={'name'}
                    value={name}
                    label="Masukan Nama kamu"
                    variant="filled"
                    onChange={handleChange}
                    color="secondary"
                    className={classes.inputBox}
                />
                <div>
                    <Typography variant={'h6'} className={classes.content}>
                        <stong>
                            Kode rahasia kamu
                        </stong>
                    </Typography>
                    <div className={classes.code}>
                        <Typography variant={'h4'} className={classes.content}>
                            {code}
                        </Typography>
                    </div>
                </div>
                <div/>
            </div>


            <div className={classes.padding}/>

            <div className={classes.padding}/>
            <div className={classes.choice}>
                <Card className={classes.cardRoot}>
                    <CardContent>
                        <Typography variant="h5" component="h2">
                            Standard Progressive Matrices Analysis
                        </Typography>

                        <Typography color="textSecondary">
                            Personality Test
                        </Typography>

                        <div className={classes.padding}/>

                        <Typography variant="body2" component="p" className={classes.cardBody}>
                            The Standard Progressive Matrices (SPM) is a group or individually administered
                            test
                            that non verbally assesses intelligence in children and adults through abstract
                            reasoning tasks.
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button value="spm"
                                onClick={() => {
                                    setTestType("spm")
                                    handleSubmitCode()
                                }}
                                variant="outlined"
                                color="secondary"
                        >
                            Start SPM assessment
                        </Button>
                    </CardActions>
                </Card>
                <div className={classes.padding}/>
                <Card className={classes.cardRoot}>
                    <CardContent>
                        <Typography variant="h5" component="h2">
                            Holland Code (RIASEC)
                        </Typography>
                        <Typography color="textSecondary">
                            Personality Test
                        </Typography>
                        <div className={classes.padding}/>
                        <Typography variant="body2" component="p" className={classes.cardBody}>
                            The Holland Codes is a system to classify jobs into job categories, interest
                            clusters, or work personality environments. In the Holland Model, these
                            categories
                            represent work personalities
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button
                            onClick={
                                () => {
                                    setTestType("riasec")
                                    handleSubmitCode()
                                }
                            }
                            variant="outlined"
                            color="secondary"
                        >
                            Start Riasec assessment
                        </Button>
                    </CardActions>
                </Card>
                <div className={classes.padding}/>
                <Card className={classes.cardRoot}>
                    <CardContent>
                        <Typography variant="h5" component="h2">
                            Cattell Culture Fair Intelligence Test (CFIT)
                        </Typography>
                        <Typography color="textSecondary">
                            Intelligence Test
                        </Typography>
                        <div className={classes.padding}/>
                        <Typography variant="body2" component="p" className={classes.cardBody}>
                            The Culture Fair Intelligence Test was created by Raymond Cattell in 1949 as an attempt
                            to measure cognitive abilities devoid of sociocultural and environmental influences
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button
                            onClick={
                                () => {
                                    setTestType("cfit")
                                    handleSubmitCode()
                                }
                            }
                            variant="outlined"
                            color="secondary"
                        >
                            Start CFIT assessment
                        </Button>
                    </CardActions>
                </Card>
            </div>
            <div className={classes.paddingBottom}/>
        </Box>
    );
};

export default Onboarding;
