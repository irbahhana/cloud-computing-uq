import React from 'react';
import Box from "@material-ui/core/Box";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Onboarding from "./onboarding";
import Landing from "./landing";


const useStyles = makeStyles((theme) => ({
    bigBox: {
        width: '100vw',
        height: '100vh',
        overflowY: 'scroll',
        background: 'linear-gradient(133.68deg, #B067CB 8.52%, #A45DCC 21.29%, #924DCD 34.67%, #823FCE 65.76%, #6727D0 85.49%);\n',
        display: 'flex',
    },
    padding: {
        paddingTop: '5rem'
    },
    title: {
        marginTop: '15rem',
        color: '#FFFFFF',

    },
    titleBox: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    content: {
        display: "flex",
    },
    desc: {
        color: "#FFFFFF",
        marginTop: '2rem',
    }
}));


const HomePage = ({code, setCode}) => {
    const classes = useStyles()
    return (
        <div>
            <Box className={classes.bigBox}>
                <div className={classes.content}>
                    {
                        code ?
                            <Onboarding code={code} setCode={setCode}/>
                            :
                            <Landing code={code} setCode={setCode}/>
                    }
                </div>
            </Box>
        </div>
    );
}

export default HomePage;
