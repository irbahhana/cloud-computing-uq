import React, {useEffect, useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import Navbar from "../components/navbar";
import Riasec from "../form/riasec";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router";
import {Prompt} from "react-router-dom";
import {isDev} from "../utils/config";
import testAtom from "../atom/testAtom";
import {useRecoilValue} from "recoil";
import Spm from "../form/spm";
import Cfit from "../form/cfit";

const useStyles = makeStyles((theme) => ({
        root: {
            width: '100vw',
            height: '100vh',
            overflowY: 'scroll',
            background: 'linear-gradient(107.56deg, #8357FF 0%, #612CD3 40.1%, #751DCB 100%);',
        },
        padding: {
            paddingTop: '5rem'
        },
        content: {
            display: "flex",
            flexDirection: "column",
            alignItems: 'center',
            justifyContent: 'center'
        },
        desc: {
            color: "#FFFFFF",
            marginTop: '2rem',
        },
        paddingOne: {
            paddingTop: '1rem'
        },
        header: {
            display: "flex",
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: "center",
        },
    }
));

function Assessment({code}) {
    const classes = useStyles()
    const history = useHistory()
    const axios = require('axios')
    const [user, setUser] = useState()
    const url = isDev() ? `http://localhost:8000/api/view/${code}` : `/api/view/${code}/`
    const testType = useRecoilValue(testAtom)

    useEffect(() => {
        axios.get(url)
            .then((response) => {
                console.log(response.data)
                setUser(response.data.name)
            })
    })

    const testPicker = () => {
        switch (testType) {
            case "spm":
                return <Spm code={code}/>
            case "riasec":
                return <Riasec code={code}/>
            case "cfit":
                return <Cfit code={code}/>
        }
    }

    function handleClick() {
        history.push('/')
    }

    if (code !== '') {
        return (
            <div>
                <Navbar/>
                <Prompt
                    message={"All unsave progress will be lost"}/>
                <div className={classes.root}>
                    <div className={classes.content}>
                        <div className={classes.padding}/>
                        <div className={classes.header}>
                            <Typography variant={"h2"}>
                                <strong style={{fontWeight: "bold", color: "#ebfd72"}}>
                                    Hello {user}
                                </strong>
                            </Typography>
                            <div className={classes.paddingOne}/>
                            <Typography variant={"h6"} style={{fontWeight: "medium", color: "white"}}>
                                Please Read each statement. There are no wrong answers!
                            </Typography>
                        </div>
                        <div className={classes.paddingOne}/>
                        {
                            testPicker()
                        }


                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div>
                <div className={classes.root}>
                    <div className={classes.content}>
                        <Navbar/>
                        <div className={classes.padding}/>
                        <CircularProgress color="secondary"/>
                        <Typography variant={"h5"} style={{color: '#FFFFFF'}}>
                            You haven't get any code yet. click the button to get the code!
                        </Typography>
                        <Button variant={"contained"} color={"secondary"} onClick={handleClick}>
                            Get Code
                        </Button>
                    </div>
                </div>
            </div>
        )
    }

}

export default Assessment;
