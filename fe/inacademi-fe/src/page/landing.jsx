import React, {useState} from 'react';
import Navbar from "../components/navbar";
import Modal from "@material-ui/core/Modal";
import {Alert, AlertTitle} from "@material-ui/lab";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {isDev} from "../utils/config";
import APIConfig from "../utils/ApiConfig";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "159px",
        paddingLeft: '120px',
        paddingRight: '120px'
    },
    title: {
        color: '#FFFFFF',
        textAlign: 'justify'
    },
    subtitle: {
        marginTop: '1rem',
        color: '#FFFFFF',
        textAlign: 'justify',
        textJustify: 'inter-word'
    },
    padding: {
        padding: '1rem'
    },
    inputBox: {
        background: '#FFFFFF',
        borderRadius: '8px',
        width: '300px',
    },
    paddingBottom: {
        padding: '3rem'
    },
    loginBox: {
        display: 'flex',
        alignItems: 'center'
    },
    codeInput: {
        marginTop: '2rem',
        display: "flex",
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    product: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    description: {
        flexDirection: "column",
        justifyContent: 'flex-start'
    },
    boyImage: {
        width: "589.07px",
        height: "462"
    },
    produkButton: {
        color: "white",
        '&.MuiButton-outlinedSecondary': {
            border: '1px solid white',
        },
        borderRadius: '8px',
    },
    productWrapper: {
        display: 'flex',
        flexDirection: 'column'
    },
    productTitle: {
        justifyContent: 'flex-start'
    },
    PBWrapper: {
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    visiMisi: {
        paddingTop:'105px',
        flexDirection: 'column',
        justifyContent: "center",
        alignItems: "center"
    },
    vision: {
        width: "200px",
        height: "200px"
    },
    visiTitle:{
        color:"#FFF"

    }
}));


const Landing = ({code, setCode}) => {
    const classes = useStyles()
    const [modalAlertLogin, setModalAlertLogin] = useState(false)
    const [codeForm, setCodeForm] = useState('')
    const [name, setName] = useState("")

    function handleChangeLogin(event) {
        setCodeForm(event.target.value)
    }

    function handleSubmit(event) {
        event.preventDefault()
        const url = isDev() ? `http://localhost:8000/api/tester/` : `/api/tester/`
        APIConfig.post(url, {
            code: "a",
            name: null,
        }).then(
            (response) => {
                console.log(response.data)
                setCode(response.data.code)
            }
        )
    }

    function submitLogin(event) {
        event.preventDefault()
        APIConfig.get(
            'view/' + codeForm
        ).then((response) => {
            if (response.data) {
                setName(response.data.name)
                setCode(response.data.code)
            } else {
                setModalAlertLogin(true)
            }
        })
    }

    return (
        <div className={classes.root}>
            <Navbar/>

            {
                modalAlertLogin ?
                    <Modal
                        open={modalAlertLogin}
                        onClose={() => setModalAlertLogin(false)}
                    >
                        <Alert severity="warning">
                            <AlertTitle>Kode tidak ditemukan</AlertTitle>
                            Kode yang dimasukan salah. Masukan kode lagi atau buat kode baru
                        </Alert>
                    </Modal>
                    :
                    null
            }
            <div className={classes.product}>
                <div className={classes.description}>
                    <Typography variant={'h4'} className={classes.title} gutterBottom>
                        <strong>
                            Apakah kamu sudah mengetahui minat bakat kamu?
                        </strong>
                    </Typography>
                    <div/>
                    <Typography variant={"body1"} className={classes.subtitle} gutterBottom>
                        PsyMe adalah lembaga layanan psikologi yang memberikan layanan psikologi berupa penyiksaan
                        psikologi secara online serta layanan konsulatasi penjurusan minat bakat dan karir
                    </Typography>
                    <div className={classes.productWrapper}>
                        <div>
                            <Typography variant={'h5'} className={classes.subtitle}>
                                <strong> Produk PsyMe</strong>
                            </Typography>
                        </div>
                        <div className={classes.PBWrapper}>
                            <Button variant={"outlined"} color={'secondary'} className={classes.produkButton}>
                                Pemeriksaan Psikologi Online
                            </Button>
                            <Button variant={"outlined"} color={'secondary'} className={classes.produkButton}>
                                Pemetaan Potensi Bakat dan Minat
                            </Button>
                            <Button variant={"outlined"} color={'secondary'} className={classes.produkButton}>
                                Konsultasi Karir
                            </Button>
                            <Button variant={"outlined"} color={'secondary'} className={classes.produkButton}>
                                Konsultasi Penjurusan Pendidikan
                            </Button>
                        </div>

                    </div>
                </div>
                <div>
                    <img src={require('../images/assets/asset1.png')} className={classes.boyImage} alt={"image"}/>
                </div>
            </div>
            <div className={classes.visiMisi}>
                <img className={classes.vision} src={require("../images/assets/vision1.png")} alt={"vision"}/>
                <Typography variant={"h4"} className={classes.visiTitle}>
                    Visi misi
                </Typography>
                <div className={classes.padding}/>
                <Typography variant={"body1"} className={classes.title}>
                    Mejadi lembaga layanan psikologi yang berfokus pada bidang pendidikan untuk membantu para siswa
                    mengenali potensi, bakat, dan minatnya agar dapat optimal di masa depan.
                </Typography>
            </div>
            <form onSubmit={handleSubmit}>
                <Button type={"submit"} variant="contained" color="secondary">
                    Start!
                </Button>
            </form>
            <div>
                <h2 className={classes.subtitle}>
                    Sudah memiliki kode?
                </h2>
                <form onSubmit={submitLogin}>
                    <div className={classes.loginBox}>
                        <TextField
                            required={true}
                            name={'code'}
                            value={codeForm}
                            label="code"
                            variant="filled"
                            onChange={handleChangeLogin}
                            color="secondary"
                            className={classes.inputBox}
                        />
                        <Button type={"submit"} variant="contained" color="secondary">
                            Start!
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Landing;
