import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import React from "react";

const riasecFactory = (arr) => {

    const realistic = ["Agriculture",
        "Health Assistant",
        "Computers",
        'Construction',
        'Mechanic/Machinist',
        'Engineering',
        'Food and Hospitality']
    const investigative = ['Marine Biology'
        , 'Engineering'
        , 'Chemistry'
        , 'Zoology'
        , 'Medicine/Surgery'
        , 'Consumer Economics'
        , 'Psychology']
    const artistic = [
        'Communications'
        , 'Cosmetology'
        , 'Fine and Performing Arts'
        , 'Photography'
        , 'Radio and TV'
        , 'Interior Design'
        , 'Architecture'
    ]
    const social = [
        'Counseling'
        , 'Nursing'
        , 'Physical Therapy'
        , 'Travel'
        , 'Advertising'
        , 'Public Relations'
        , 'Education'
    ]

    const enterprising = [
        'Fashion Merchandising'
        , 'Real Estate'
        , 'Marketing/Sales'
        , 'Law'
        , 'Political Science'
        , 'International Trade'
        , 'Banking/Finance'
    ]

    const conventional = [
        'Accounting'
        , 'Court Reporting'
        , 'Insurance'
        , 'Administration'
        , 'Medical Records'
        , 'Banking'
        , 'Data Processing'
    ]
    return arr.map((q) => {
        if (q.id === 'r') {
            return (
                <div style={{paddingTop: '1rem'}}>
                    <Card style={
                        {
                            minWidth: 275,
                            width: '20rem',
                            backgroundColor:"#ffadad"
                        }
                    }>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                You are a
                                <Typography variant="h5" style={{fontWeight:"bold"}} component="h2">
                                    REALISTIC
                                </Typography>
                                type of person!
                            </Typography>
                            <div style={{paddingTop:'1rem'}}/>
                            <Typography variant="body2" component="p">
                                You are good at mechanical or athletic
                                jobs. Good college majors for Realistic people are...
                                <div>
                                    <List dense={true} component="nav">
                                        {realistic.map((q) => {
                                            return (
                                                <ListItem button>
                                                    <ListItemText
                                                        primary={q}
                                                    />
                                                </ListItem>
                                            )
                                        })}
                                    </List>
                                </div>
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
            )
        } else if (q.id === 'i') {
            return (
                <div style={{paddingTop: '1rem'}}>
                    <Card style={
                        {
                            minWidth: 275,
                            width: '20rem',
                            backgroundColor:'#ffd6a5'
                        }
                    }>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                You are an
                                <Typography variant="h5" style={{fontWeight:"bold"}} component="h2">
                                    INVESTIGATIVE
                                </Typography>
                                type of person!
                            </Typography>
                            <div style={{paddingTop:'1rem'}}/>
                            <Typography variant="body2" component="p">
                                You like to watch, learn, analyze and solve
                                problems. Good college majors for Investigative
                                people are…

                                <div>
                                    <List dense={true} component="nav">
                                        {investigative.map((q) => {
                                            return (
                                                <ListItem button>
                                                    <ListItemText
                                                        primary={q}
                                                    />
                                                </ListItem>
                                            )
                                        })}
                                    </List>
                                </div>
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
            )
        } else if (q.id === 'a') {
            return (
                <div style={{paddingTop: '1rem'}}>
                    <Card style={
                        {
                            minWidth: 275,
                            width: '20rem',
                            backgroundColor:'#fdffb6'
                        }
                    }>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                You are a
                                <Typography variant="h5" style={{fontWeight:"bold"}} component="h2">
                                    ARTISTIC
                                </Typography>
                                type of person!
                            </Typography>
                            <div style={{paddingTop:'1rem'}}/>
                            <Typography variant="body2" component="p">
                                You like to work in unstructured situations
                                where they can use their creativity. Good majors for
                                Artistic people are…
                                <div>
                                    <List dense={true} component="nav">
                                        {artistic.map((q) => {
                                            return (
                                                <ListItem button>
                                                    <ListItemText
                                                        primary={q}
                                                    />
                                                </ListItem>
                                            )
                                        })}
                                    </List>
                                </div>
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
            )
        } else if (q.id === 's') {
            return (
                <div style={{paddingTop: '1rem'}}>
                    <Card style={
                        {
                            minWidth: 275,
                            width: '20rem',
                            backgroundColor:"#caffbf"
                        }
                    }>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                You are a
                                <Typography variant="h5" style={{fontWeight:"bold"}} component="h2">
                                    SOCIAL
                                </Typography>
                                type of person!
                            </Typography>
                            <div style={{paddingTop:'1rem'}}/>
                            <Typography variant="body2" component="p">
                                You like to work with other people,
                                rather than things. Good college majors for
                                Social people are…
                                <div>
                                    <List dense={true} component="nav">
                                        {social.map((q) => {
                                            return (
                                                <ListItem button style={{
                                                    "&:hover": {
                                                        backgroundColor: "#FFF"},
                                                }}>
                                                    <ListItemText
                                                        primary={q}
                                                    />
                                                </ListItem>
                                            )
                                        })}
                                    </List>
                                </div>
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
            )
        } else if (q.id === 'e') {
            return (
                <div style={{paddingTop: '1rem'}}>
                    <Card style={
                        {
                            minWidth: 275,
                            width: '20rem',
                            backgroundColor:'#9bf6ff'
                        }
                    }>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                You are a
                                <Typography variant="h5" style={{fontWeight:"bold"}} component="h2">
                                    ENTERPRISING
                                </Typography>
                                type of person!
                            </Typography>
                            <div style={{paddingTop:'1rem'}}/>
                            <Typography variant="body2" component="p">
                                You like to work with others and enjoy
                                persuading and and performing. Good college majors
                                for Enterprising people are:
                                <div>
                                    <List dense={true} component="nav">
                                        {enterprising.map((q) => {
                                            return (
                                                <ListItem button>
                                                    <ListItemText
                                                        primary={q}
                                                    />
                                                </ListItem>
                                            )
                                        })}
                                    </List>
                                </div>
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
            )
        } else if (q.id === 'c') {
            return (
                <div style={{paddingTop: '1rem'}}>
                    <Card style={
                        {
                            minWidth: 275,
                            width: '20rem',
                            backgroundColor:'#bdb2ff'
                        }
                    }>
                        <CardContent>
                            You are a
                            <Typography variant="h5" style={{fontWeight:"bold"}} component="h2">
                                CONVENTIONAL
                            </Typography>
                            type of person!
                            <Typography variant="body2" component="p">
                                You are a very detail oriented,organized
                                and like to work with data. Good college majors
                                for Conventional people are:
                                <div>
                                    <List dense={true}>
                                        {conventional.map((q) => {
                                            return (
                                                <ListItem button>
                                                    <ListItemText
                                                        primary={q}
                                                    />
                                                </ListItem>
                                            )
                                        })}
                                    </List>
                                </div>
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
            )
        }
    })
}

export default riasecFactory
