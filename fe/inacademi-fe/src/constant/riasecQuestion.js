export const riasecQuestions = [
    {
        id:0,
        question:"I like to work on cars"
    },
    {
        id:1,
        question:"I like to do puzzles"
    },
    {
        id:2,
        question:"I am good at working independently"

    },
    {
        id:3,
        question:"I like to work in teams"
    },
    {
        id:4,
        question:"I am an ambitious person, I set goals for myself"
    },
    {
        id:5,
        question:"I like to organize things, (files, desks/offices)"
    },
    {
        id:6,
        question:"I like to build things"
    },
    {
        id:7,
        question:"I like t o rad about art and music"
    },
    {
        id:8,
        question:"I like to have clear instructions to follow"
    },
    {
        id:9,
        question:"I like to try to influence or persuade people"
    },
    {
        id:10,
        question:"I like to do experiments"
    },
    {
        id:11,
        question:"I like to teach or train people"
    },
    {
        id:12,
        question:"I like trying to help people solve their problems"
    },
    {
        id:13,
        question:"I like to take care of animals"
    },
    {
        id:14,
        question:"I wouldn’t mind working 8 hours per day in an office"
    },
    {
        id:15,
        question:"I like selling things"
    },
    {
        id:16,
        question:"I enjoy creative writing"
    },
    {
        id:17,
        question:"I enjoy science"
    },
    {
        id:18,
        question:"I am quick to take on new responsibilities"
    },
    {
        id:19,
        question:"I am interested in healing people"
    },
    {
        id:20,
        question:"I enjoy trying to figure out how things work"
    },
    {
        id:21,
        question:"I like putting things together or assembling things"
    },
    {
        id:22,
        question:"I am a creative person"
    },
    {
        id:23,
        question:"I pay attention details"

    },
    {
        id:24,
        question:"I like to do filing or typing"
    },
    {
        id:25,
        question:"I like to analyze things (problems/ situations)"
    },
    {
        id:26,
        question:"I like to play instruments or sing"
    },
    {
        id:27,
        question:"I enjoy learning about other cultures"
    },
    {
        id:28,
        question:"I would like to start my own business"
    },
    {
        id:29,
        question:"I like to cook"
    },
    {
        id:30,
        question:"I like acting in plays"
    },
    {
        id:31,
        question:"I am a practical person"
    },
    {
        id:32,
        question:"I like working with numbers or charts"
    },
    {
        id:33,
        question:"I like to get into discussions about issues"
    },
    {
        id:34,
        question:"I am good at keeping records of my work"
    },
    {
        id:35,
        question:"I like to lead"
    },
    {
        id:36,
        question:"I like working outdoor"
    },
    {
        id:37,
        question:"I would like to work in an office"
    },
    {
        id:38,
        question:"I’m good at math"
    },
    {
        id:39,
        question:"I like helping people"
    },
    {
        id:40,
        question:"I like to draw"
    },
    {
        id:41,
        question:"I like to work on cars"
    }
]
