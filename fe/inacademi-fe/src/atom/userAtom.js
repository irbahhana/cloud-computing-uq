import {atom} from "recoil";

const userAtom = atom({
    key:'hasCode',
    default:{hasCode:''}
})

export default userAtom;