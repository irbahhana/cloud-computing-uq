import {atom} from "recoil";

const testAtom = atom({
    key: 'testType',
    default: ''
})

export default testAtom;