import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import FormHelperText from "@material-ui/core/FormHelperText";

const useStyles = makeStyles((theme) => ({
    titleWrapper: {
        display: 'flex',
        alignItems: "center",
        justifyContent: "center"
    }
}));

export default function InstructionModal({open, setOpen, content, testType, setStartTimer}) {

    const classes = useStyles()
    const handleClose = () => {
        if ([a, b, c].filter((v) => v).length === 3) {
            setStartTimer(true)
            setOpen(false);
        } else {
            alert("Harap checklist semua item")
        }
    };

    const [checkboxState, setCheckboxState] = useState({
        a: false,
        b: false,
        c: false
    })

    const handleChange = (event) => {
        setCheckboxState({...checkboxState, [event.target.name]: event.target.checked});
    };


    const {a, b, c} = checkboxState
    const error = [a, b, c].filter((v) => v).length !== 3;
    return (
        <div>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <div className={classes.titleWrapper}>
                    <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                        {testType} Test
                    </DialogTitle>
                </div>

                <DialogContent dividers>
                    <Typography gutterBottom>
                        {content.desc}
                    </Typography>
                    <Typography gutterBottom>
                        {content.instruction}
                    </Typography>
                </DialogContent>

                <DialogContent dividers>
                    <FormControl required error={error} component="fieldset" className={classes.formControl}>
                        <FormLabel component="legend">Dengan ini saya menyatakan</FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox checked={a} onChange={handleChange} name="a"/>}
                                label="Akan mengerjakan test ini tanpa bantuan orang lain"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={b} onChange={handleChange} name="b"/>}
                                label="Akan mengerjakan test ini tanpa bantuan dari Internet maupun faktor external lain"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={c} onChange={handleChange} name="c"/>}
                                label="Akan menerima hasil test saya"
                            />
                        </FormGroup>
                        <FormHelperText>Harap di checklist semuanya</FormHelperText>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Saya siap mengerjakan
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
