import React from 'react';
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '386px',
        background: 'white',
        display: 'flex',
        flexDirection: 'column',
        padding: '21px',
        borderRadius: '10px',
    },
    buttonContainer: {
        display: 'flex',
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        flexItems: 'margin-left'
    },
    questionButton: {
        maxWidth: '36px',
        maxHeight: '36px',
        minWidth: '36px',
        minHeight: '36px'
    },
    answeredButton: {
        maxWidth: '36px',
        maxHeight: '36px',
        minWidth: '36px',
        minHeight: '36px',
        background: '#8743CE'
    },
    buttonMargin: {
        padding: '5px'
    },
    title: {
        display: 'flex',
        justifyContent: 'flex-start',
        paddingBottom: '1rem'
    }
}));


const PartQuestionPicker = ({questions, questionPointer, setQuestionPointer, radioValue, part}) => {
    const classes = useStyles()

    function changeQuestion(questionTarget) {
        setQuestionPointer(questionTarget)
    }

    return (
        <div className={classes.root}>
            <div className={classes.title}>
                <Typography variant={'h5'}>
                    Navigasi Soal
                </Typography>
            </div>

            <div className={classes.buttonContainer}>
                {
                    questions.length > 0 ?
                        questions.map((q) => {
                            return (
                                <div className={classes.buttonMargin}>
                                    {
                                        q.id === questionPointer ?
                                            <Button className={classes.questionButton} id={q.id} variant="contained"
                                                    onClick={() => changeQuestion(q.id)} disabled={true}>
                                                <strong>
                                                    {q.id + 1}
                                                </strong>
                                            </Button>
                                            :
                                            <Button className={radioValue[q.id].value === '' ||  isNaN(radioValue[q.id].value) ?
                                                classes.questionButton :
                                                classes.answeredButton
                                            } id={q.id} variant="contained"
                                                    onClick={() => changeQuestion(q.id)}>
                                                <strong style={
                                                    radioValue[q.id].value === '' ||  isNaN(radioValue[q.id].value) ?
                                                        {color: 'black'}
                                                        :
                                                        {color: 'white'}
                                                }>
                                                    {q.id + 1}
                                                </strong>
                                            </Button>
                                    }
                                </div>
                            )
                        })
                        :
                        <div>
                            <p>
                                Loading...
                            </p>
                        </div>
                }
                {console.log(radioValue[questionPointer])}
            </div>
        </div>
    );
};

export default PartQuestionPicker;
