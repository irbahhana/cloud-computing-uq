import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { useHistory} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    bar: {
        background: '#6B2CD3',
    },
    grow: {
        flexGrow: 1
    },
    margins:{
        display:'flex',
        flexGrow: 1,
        paddingLeft:'80px',
        paddingRight:'80px'
    }
}));

function ElevationScroll(props) {
    const {children} = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
    });

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0,
    });
}

function Navbar(props) {
    const classes = useStyles();
    const history = useHistory()

    function handleClick() {
        history.push('/view')
    }
    function handleClickHome() {
        history.push('/')
    }

    return (
        <ElevationScroll {...props}>
            <AppBar className={classes.bar}>
                <Toolbar>
                    <div className={classes.margins}>
                        <Button color={"inherit"} onClick={handleClickHome} >Inacademi</Button>
                        <div className={classes.grow}/>

                        <Button variant={"contained"} onClick={handleClick} color="secondary" disableElevation>
                            View Result
                        </Button>
                    </div>
                </Toolbar>
            </AppBar>
        </ElevationScroll>
    );
}

export default Navbar;
