import React from 'react';
import Timer from "react-compound-timer";
import APIConfig from "../utils/ApiConfig";
import {useHistory} from "react-router";

const TestTimer = ({initial, radioValue, code}) => {
    const history = useHistory()
    const forceSubmit = () => {
        const answers = radioValue.map((radioValue) => {
            radioValue.value = parseInt(radioValue.value)
            }
        )
        console.log(answers)
        APIConfig.post(
            "/spm/submit",
            {
                'answers': radioValue,
                'code': code
            }
        ).then(
            (res) => {
            }
        ).then(
            history.push('/view')
        )
    }

    return (
        <div>
            <Timer
                initialTime={initial}
                direction="backward"
                startImmediately={true}
                checkpoints={[
                    {
                        time: 0,
                        callback: () => forceSubmit()
                    }
                ]}
            >
                {
                    () => (
                        <div>
                            <Timer.Minutes/>:<Timer.Seconds/>
                        </div>
                    )
                }
            </Timer>
        </div>
    );
};

export default TestTimer;
