import React, {useState} from 'react';
import './App.css';
import CssBaseline from "@material-ui/core/CssBaseline";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import HomePage from "./page/homepage";
import Result from "./page/result";
import Assessment from "./page/assessment";


function App() {
    const [code, setCode] = useState('')
    const [testType, setTestType] = useState('')
    return (
        <React.Fragment>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"/>
            <CssBaseline/>
            <div className="App">
                <BrowserRouter>
                    <Switch>
                        <Route path={'/'} exact={true}
                               render={() => (
                                   <HomePage code={code} setCode={setCode} testType={testType} setTestType={setTestType}/>
                               )}/>

                        <Route path={'/assessment'} exact={true}
                               render={() => (
                                   <Assessment code={code}/>
                               )}/>
                        <Route path={'/view'} exact={true}
                               render={() => (<Result code={code} setCode={setCode}/>
                               )}/>
                    </Switch>
                </BrowserRouter>
            </div>
        </React.Fragment>
    );
}

export default App;
