import React, {useEffect, useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useHistory} from "react-router";
import {isDev} from "../utils/config";
import PartQuestionPicker from "../components/partQuestionPicker";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";

const useStyles = makeStyles(() => ({
    formWrapper: {
        display: 'flex',
        flexDirection: "row",
    },

    buttonWrapper: {
        display: 'flex',

        flexDirection: 'column'
    },

    cardPadding: {
        padding: '1rem'
    },
    bb: {
        display: 'flex',
        flexDirection: 'column',
    },
    paddingBottom: {
        padding: '3rem'
    },
    buttonSubmit: {
        backgroundColor: 'green',
        color: 'white'
    },
    questionImg: {
        width: "40rem",
        height: "20rem"
    },
    questionImgSmall: {
        width: "40rem",
        height: "10rem"
    }
}));

const initial = []
for (let i = 0; i <= 49; i++) {
    initial.push({id: String(i), value: String('')})
}

const Cfit = ({code}) => {
    const classes = useStyles()
    const axios = require('axios').default;
    const [questions, setQuestions] = useState([])
    const [radioValue, setRadioValue] = useState(initial)
    const [questionPointer, setQuestionPointer] = useState(0)
    const history = useHistory()
    const questionContainer = []

    const handleChange = (event) => {
        let data = [...radioValue]
        const id = event.target.name
        const value = event.target.value
        data[parseInt(id)] = {
            id: id, value: value
        }
        setRadioValue(data)
    };

    useEffect(() => {
        for (let i = 1; i < 5; i++) {
            loader(i)
        }
        setQuestions(questionContainer)
        console.log(radioValue)
    }, [])

    function handleSubmit(event) {
        event.preventDefault()
        const url = isDev() ? `http://localhost:8000/api/cfit/submit/` : `/api/cfit/submit/`
        radioValue.map((radioValue) => {
                radioValue.value = parseInt(radioValue.value)
            }
        )

        axios.post(url, {
            'answers': radioValue,
            'code': code
        },)
            .then((res) => {
                    console.log(res.data)
                    console.log(res.response)
                }
            ).then(() => history.push('/view'))
    }

    function questionLoader(question_ammount, start, part) {
        let idQuestion = start
        for (let i = 1; i <= question_ammount; i++) {
            const questionImage = require("../images/cfit/g" + part.toString() + "/a" + i.toString() + ".png")
            questionContainer.push(
                {
                    id: idQuestion,
                    q: questionImage
                }
            )
            idQuestion++
        }
    }

    function loader(part) {
        switch (part) {
            case 1:
                questionLoader(13, 0, 1)
                break
            case 2:
                questionLoader(14, 13, 2)
                break
            case 3:
                questionLoader(13, 27, 3)
                break
            case 4:
                questionLoader(10, 40, 4)
                break
        }
    }

    return (
        <>
            <div className={classes.formWrapper}>
                <form onSubmit={handleSubmit}>
                    {
                        questions && questions.length > 0 ?
                            <>
                                <Card variant="outlined">
                                    <CardContent className={classes.bb}>
                                        <FormControl component="fieldset">

                                            <img className={classes.questionImgSmall}
                                                 src={questions[questionPointer].q}
                                                 key={"question " + questions[questionPointer].id}
                                                 alt={"question" + questions[questionPointer].id}
                                            />

                                            <RadioGroup key={"q" + questions[questionPointer].id}
                                                        name={questions[questionPointer].id}
                                                        value={radioValue[questionPointer].value}
                                                        onChange={handleChange}>
                                                <FormControlLabel value="1" control={<Radio/>} label="A"/>
                                                <FormControlLabel value="2" control={<Radio/>} label="B"/>
                                                <FormControlLabel value="3" control={<Radio/>} label="C"/>
                                                <FormControlLabel value="4" control={<Radio/>} label="D"/>
                                                <FormControlLabel value="5" control={<Radio/>} label="E"/>
                                                <FormControlLabel value="6" control={<Radio/>} label="F"/>

                                            </RadioGroup>
                                        </FormControl>
                                    </CardContent>
                                </Card>
                            </>
                            :
                            <>
                                <h1>Loading...</h1>
                            </>
                    }
                    <PartQuestionPicker radioValue={radioValue} questions={questions} questionPointer={questionPointer}
                                        setQuestionPointer={setQuestionPointer} part={4}/>
                </form>
            </div>
            <div className={classes.paddingBottom}/>
        </>
    );
};

export default Cfit;
