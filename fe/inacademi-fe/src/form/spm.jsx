import React, {useEffect, useRef, useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import APIConfig from "../utils/ApiConfig";
import {useHistory} from "react-router";
import PartQuestionPicker from "../components/partQuestionPicker";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import InstructionModal from "../components/instructionModal";
import {spm} from "../constant/instruction"
import TestTimer from "../components/testTimer";

const useStyles = makeStyles((theme) => ({

    root: {
        width: '100vw',
        height: '100vh',
        overflowY: 'scroll',
        background: 'linear-gradient(107.56deg, #8357FF 0%, #612CD3 40.1%, #751DCB 100%);',
    },
    padding: {
        paddingTop: '5rem'
    },
    title: {
        marginTop: '15rem',
        color: '#FFFFFF'
    },
    titleBox: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    content: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    desc: {
        color: "#FFFFFF",
        marginTop: '2rem',
    },
    formWrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },

    buttonWrapper: {
        display: 'flex',

        flexDirection: 'column'
    },

    cardPadding: {
        padding: '1rem'
    },
    bb: {
        display: 'flex',
        flexDirection: 'column',
    },
    paddingBottom: {
        padding: '3rem'
    },
    buttonSubmit: {
        backgroundColor: 'green',
        color: 'white'
    },
    questionImg: {
        width: "20rem",
        height: "13rem",

    },
    answerImg: {
        width: "9rem",
        height: "5rem"
    },
    paddingOne: {
        padding: "1rem"
    },
    cardStyle: {
        width: '50%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    pickerWrapper: {
        display: "flex",
        flexDirection: 'column'
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
}));

const initial = []


for (let i = 0; i <= 59; i++) {
    initial.push({id: String(i), value: String("")})
}


const Spm = (code) => {
    const classes = useStyles()
    const [questions, setQuestion] = useState([])
    const [radioValue, setRadioValue] = useState(initial)
    const [questionPointer, setQuestionPointer] = useState(0)
    const [open, setOpen] = useState(false)
    const [startTimer, setStartTimer] = useState(false)
    const formRef = useRef(null)
    const [timeUp, setTimeUp] = useState(false)
    const history = useHistory()
    const qq = []
    let aa = []
    let temp = []

    function loopLoader(part) {
        if (part <= 2) {
            for (let j = 1; j < 73; j++) {
                const a = require("../images/spm/p" + part.toString() + "/jawab/a" + j.toString() + ".png")
                if (!(j % 6 === 0)) {
                    temp.push(a)
                } else {
                    temp.push(a)
                    aa.push(temp)
                    temp = []
                }
            }
        } else {
            console.log(part)
            for (let j = 1; j < 97; j++) {
                const a = require("../images/spm/p" + part.toString() + "/jawab/a" + j.toString() + ".png")
                if (!(j % 8 === 0)) {
                    temp.push(a)
                } else {
                    temp.push(a)
                    aa.push(temp)
                    temp = []
                }
            }
        }
        switch (part) {
            case 1:
                questionLoader(0, 1)
                break
            case 2:
                questionLoader(12, 2)
                break
            case 3:
                questionLoader(24, 3)
                break
            case 4:
                questionLoader(36, 4)
                break
            case 5:
                questionLoader(48, 5)
                break
        }
        aa = []
    }

    function questionLoader(start, part) {
        let idCounter = start
        for (let i = 1; i < 13; i++) {
            const q = require("../images/spm/p" + part.toString() + "/soal/s" + i.toString() + ".png")
            qq.push({
                id: idCounter,
                q: q,
                a: aa[i - 1]
            })
            idCounter++
        }
    }


    function handleSubmit(event) {
        event.preventDefault()
        const answers = radioValue.map((radioValue) => {
                radioValue.value = parseInt(radioValue.value)
                return radioValue
            }
        )
        APIConfig.post(
            "/spm/submit",
            {
                'answers': radioValue,
                'code': code
            }
        ).then(
            (res) => {
            }
        ).then(
            history.push('/view')
        )
    }




    function loader(part) {

        switch (part) {
            case 1:
                loopLoader(part)
                break
            case 2:
                loopLoader(part)
                break
            case 3:
                loopLoader(part)
                break
            case 4:
                loopLoader(part)
                break
            case 5:
                loopLoader(part)
                break
        }
    }

    const handleChange = (event) => {
        let data = [...radioValue]
        const id = event.target.name
        const value = event.target.value

        data[parseInt(id)] = {
            id: id, value: value
        }
        setRadioValue(data)
    };

    const cleanAnswer = () => {
        for (let i = 0; i < radioValue.length; i++) {
            if (isNaN(radioValue[i].value)) {
                radioValue[i].value = ""
            }
        }
    }


    useEffect(() => {
        for (let i = 1; i < 6; i++) {
            loader(i)
        }
        setQuestion(qq)
        cleanAnswer()
        setOpen(true)
    }, [])


    return (
        <>
            <InstructionModal open={open} setOpen={setOpen} content={spm} testType={"SPM"}
                              setStartTimer={setStartTimer}/>
            <form ref={formRef} onSubmit={handleSubmit}>
                {
                    questions && questions.length > 0 ?
                        <>
                            <div className={classes.formWrapper}>
                                <Card variant="outlined" className={classes.cardStyle}>
                                    <CardContent className={classes.bb}>
                                        <FormControl component="fieldset">
                                            <img className={classes.questionImg} src={questions[questionPointer].q}
                                                 key={"question " + questions[questionPointer].id}
                                                 alt={"question" + questions[questionPointer].id}
                                            />
                                            <div className={classes.paddingOne}/>
                                            <RadioGroup row
                                                        key={"q" + questions[questionPointer].id}
                                                        name={questions[questionPointer].id}
                                                        value={(radioValue[questions[questionPointer].id].value).toString()}
                                                        onChange={handleChange}>
                                                {
                                                    questions[questionPointer].a.map((radioAnswer, index) => {
                                                        return (
                                                            <FormControlLabel value={(index + 1).toString()}
                                                                              control={<Radio/>}
                                                                              label={
                                                                                  <img
                                                                                      className={classes.answerImg}
                                                                                      alt={questions[questionPointer].id.toString() + index}
                                                                                      src={radioAnswer}/>
                                                                              }/>
                                                        )
                                                    })
                                                }
                                            </RadioGroup>
                                        </FormControl>
                                    </CardContent>
                                </Card>
                                <div className={classes.paddingOne}/>
                                <div className={classes.pickerWrapper}>
                                    <div style={{color: "white"}}>
                                        <TestTimer initial={300 * 1000} radioValue={radioValue} code={code}/>
                                    </div>
                                    <PartQuestionPicker questions={questions} questionPointer={questionPointer}
                                                        setQuestionPointer={setQuestionPointer} part={5}
                                                        radioValue={radioValue}
                                                        questionCountPart={12}/>
                                    <div className={classes.paddingOne}/>

                                    <div className={classes.buttonContainer}>
                                        <Button variant="contained" color='success'
                                                onClick={() => setQuestionPointer(questionPointer - 1 !== -1 ? questionPointer - 1 : 0)}
                                        >
                                            previous
                                        </Button>
                                        <Button variant="contained" color='success'
                                                onClick={() => setQuestionPointer(questionPointer === 59 ? 59 : questionPointer + 1)}
                                        >
                                            next
                                        </Button>
                                        <Button variant="contained" color='secondary' type="submit">
                                            Selesai Mengerjakan
                                        </Button>
                                        {
                                            console.log(questionPointer)
                                        }
                                    </div>
                                </div>

                            </div>
                        </>
                        :
                        null
                }

            </form>
            <div className={classes.paddingBottom}/>
        </>
    );
};

export default Spm;
