import React, {useEffect, useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import FormControl from "@material-ui/core/FormControl";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import {useHistory} from "react-router";
import {isDev} from "../utils/config";
import {riasecQuestions} from "../constant/riasecQuestion";
import QuestionPicker from "../components/questionPicker";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";


const useStyles = makeStyles((theme) => ({
    cardPadding: {
        padding: '1rem'
    },
    bb: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        width: '780px',
        height: '477px',
        borderRadius: '20px',
    },
    paddingBottom: {
        padding: '3rem'
    },
    buttonSubmit: {
        backgroundColor: 'green',
        color: 'white'
    },
    question: {
        color: '#2f3138'
    },
    qfMargin: {
        paddingRight: '34px'
    },
    rightContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    formContainer:{
        display:'flex',
        flexDirection: 'row',
    }
}));

const initial = []
for (let i = 0; i <= 41; i++) {
    initial.push({id: String(i), value: String('')})
}

function Riasec({code}) {
    const classes = useStyles()
    const axios = require('axios').default;
    const [questions, setQuestions] = useState([])
    const [radioValue, setRadioValue] = useState(initial)
    const [questionPointer, setQuestionPointer] = useState(0)

    const history = useHistory()
    const handleChange = (event) => {
        let data = [...radioValue]
        const id = event.target.name
        const value = event.target.value
        data[parseInt(id)] = {
            id: id, value: value
        }
        setRadioValue(data)
    };

    useEffect(() => {
        setQuestions(riasecQuestions)
    }, [questions])

    function handleSubmit(event) {
        event.preventDefault()
        const url = isDev() ? `http://localhost:8000/api/riasec/submit/` : `/api/riasec/submit/`

        axios.post(url, {
            'answers': radioValue,
            'code': code
        }, {})
            .then((res) => {
                    console.log(res.data)
                }
            ).then(() => history.push('/view'))
            .catch(() => {
                    alert("Tolong isi semua soal, jangan ada yg di kosongkan")
                }
            )
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div className={classes.formContainer}>
                    <Card variant="outlined">
                        <CardContent className={classes.bb}>
                            <FormControl component="fieldset">
                                {
                                    questions && questions.length > 0 ?
                                        <>
                                            <h1 className={classes.question}>
                                                {questions[questionPointer].id + 1}. {questions[questionPointer].question}
                                            </h1>
                                            <RadioGroup key={"q" + questionPointer} name={questionPointer}
                                                        value={radioValue[questionPointer].value}
                                                        onChange={handleChange}>
                                                <FormControlLabel value="2" control={<Radio/>} label="Agree"/>
                                                <FormControlLabel value="1" control={<Radio/>} label="Neutral"/>
                                                <FormControlLabel value="0" control={<Radio/>} label="Disagree"/>
                                            </RadioGroup>
                                        </> :
                                        <h1>
                                            Loading...
                                        </h1>
                                }
                            </FormControl>
                        </CardContent>
                    </Card>

                    <div className={classes.qfMargin}/>
                    <div className={classes.rightContainer}>
                        {
                            questions.length > 0 ?
                                <QuestionPicker questions={questions} questionPointer={questionPointer}
                                                setQuestionPointer={setQuestionPointer} radioValue={radioValue}
                                />
                                :
                                null
                        }
                        <div className={classes.buttonContainer}>
                            <Button variant="contained" color='primary'
                                    onClick={() => setQuestionPointer(questionPointer - 1 !== -1 ? questionPointer - 1 : 0)}
                            >
                                previous
                            </Button>
                            <Button variant="contained" color='primary'
                                    onClick={() => setQuestionPointer(questionPointer + 1 !== 42 ? questionPointer + 1 : 41)}
                            >
                                next
                            </Button>
                            <Button variant="contained" color='secondary' type="submit">
                                Finish attempt
                            </Button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default Riasec;
