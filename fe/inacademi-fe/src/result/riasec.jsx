import React from 'react';
import Typography from "@material-ui/core/Typography";
import riasecFactory from "../constant/riasecFactory";
import {VictoryPie} from "victory";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
    padding: {
        paddingTop: '5rem'
    },
    paddingOne: {
        paddingTop: '1rem'
    },
    paperContent: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    riasecCard:{
        display:"flex",
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    chart:{
        display:'flex',
        width:'100%',
        alignItem:'center',
        justifyContent:'center',
        height:"40rem"
    },
    wrapper:{
        padding:"1rem"
    }
}));

const Riasec = ({riasec}) => {
    function getBestThree() {
        if (riasec !== null) {
            const all = [{id: "r", value: riasec.r}, {id: "i", value: riasec.i}, {
                id: "a",
                value: riasec.a
            }, {id: "s", value: riasec.s}, {id: "e", value: riasec.e}, {id: "c", value: riasec.c}]

            const allsorted = [...all].sort((a, b) => b.value - a.value).slice(0, 3)
            return allsorted
        } else {
            return null;
        }
    }

    const data = riasec ? [
        {x: "Realistic", y: riasec.r},
        {x: "Investigative", y: riasec.i},
        {x: "Artistic", y: riasec.a},
        {x: "Social", y: riasec.s},
        {x: "Enterprising", y: riasec.e},
        {x: "Conventional", y: riasec.c},
    ] : null;
    const bestThreeToShow = getBestThree()
    const classes = useStyles();
    return (
        <>
            <Paper className={classes.wrapper}>
            {
                riasec ?
                    <>



                        <Typography variant={'h4'}>
                            Hasil Test RIASEC
                        </Typography>
                        <div className={classes.paperContent}>
                            <div className={classes.riasecCard}>
                                {riasecFactory(bestThreeToShow)}
                            </div>

                            <div className={classes.paddingOne}/>
                            <Typography variant={'h5'}>
                                Holland Code (RIASEC)
                            </Typography>
                            <div className={classes.paddingOne}/>
                            <div className={classes.chart}>
                                <VictoryPie
                                    colorScale={["#ffadad", "#ffd6a5", "#fdffb6", "#caffbf", "#9bf6ff", "#bdb2ff"]}
                                    data={data}
                                    animate={{
                                        duration: 2000,
                                        onEnter: {
                                            easing: "bounce",
                                        }
                                    }}
                                    height={500}
                                    width={600}
                                />
                            </div>

                        </div>
                    </>
                    :
                    <Typography variant={'h5'}>
                        You haven't take RIASEC test yet
                    </Typography>
            }
            </Paper>
        </>
    );
};

export default Riasec;
