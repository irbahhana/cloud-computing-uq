import React from 'react';
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
    background: {
        height: '60px',
        width: '560px',
        borderRadius: '10px',
        display:"flex",
        justifyContent:"center",
        alignItems:"center"
    }
}));

const Spm = ({spm}) => {
    const classes = useStyles()
    return (

        <>
            <Paper className={classes.background}>
                {
                    spm ?
                        <>
                            <Typography variant={'h6'}>
                                Standard Progressive Matrices Analysis Score (SPM) IQ:{spm.iq}
                            </Typography>
                        </>
                        :
                        <Typography variant={'h6'}>
                            You haven't take SPM test yet
                        </Typography>
                }
            </Paper>
        </>
    );
};

export default Spm;
