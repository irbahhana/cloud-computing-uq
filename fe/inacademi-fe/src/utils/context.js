import { createContext, useContext } from "react";

export const useAuthContext = () => useContext(AuthContext);

const AuthContext = createContext(null);

export default AuthContext;
