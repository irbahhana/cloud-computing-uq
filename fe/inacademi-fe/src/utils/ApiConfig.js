import axios from "axios";
import {isDev} from "./config";

const APIConfig = axios.create({
    baseURL: isDev() ? `http://localhost:8000/api` : `/api`,
    proxy: false
});

export default APIConfig;