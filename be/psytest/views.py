import json
import string
import random

from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .assessor import *
from .models import *
from .report_generator import ReportGenerator


def id_generator():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))


# Create your views here.
def index(request):
    return HttpResponse('A surprise to be sure, but a welcome one')


@api_view(['POST'])
def create_tester(request):
    create_user = id_generator()

    return Response({"code": create_user}, status=status.HTTP_201_CREATED)


@api_view(['GET'])
def get_user_details(request, code):
    if request.method == 'GET':
        try:
            user = Tester.objects.get(pk=code)
            json_data = {
                "code": user.code,
                "name": user.name,
            }
        except Tester.DoesNotExist:
            json_data = None
        print(json_data)
        return Response(data=json_data, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_result(request, code):
    if request.method == 'GET':
        json_data = {}
        try:
            user = Tester.objects.get(pk=code)
        except Tester.DoesNotExist:
            json_data = None
            return Response(data=json_data, status=status.HTTP_404_NOT_FOUND)

        try:
            riasec = Riasec.objects.get(code=user)

            json_data['riasec'] = {
                "result": riasec.result,
                "r": riasec.R,
                "i": riasec.I,
                "a": riasec.A,
                "s": riasec.S,
                "e": riasec.E,
                "c": riasec.C
            }
        except Riasec.DoesNotExist:
            json_data['riasec'] = None

        try:
            spm = Spm.objects.get(code=user)
            json_data['spm'] = {
                "result": spm.result,
                "iq": spm.iq
            }
        except Spm.DoesNotExist:
            json_data['spm'] = None

        try:
            cfit = Cfit.objects.get(code=user)
            json_data['cfit'] = {
                "result": cfit.score,
                "iq": cfit.iq
            }
        except Cfit.DoesNotExist:
            json_data['cfit'] = None

        return Response(data=json_data)


@api_view(['POST', 'GET', 'PUT'])
def tester(request, code):
    if request.method == 'PUT':
        tester_model = Tester(code=code)
        tester_model.name = request.POST.get('name')
        tester_model.save()
        return Response({"code": tester_model.code, 'name': tester_model.name})


@api_view(['POST'])
def riasec_answer(request):
    if request.method == 'POST':
        answer_sheet = request.body
        p = answer_sheet.decode('utf-8')
        p_load = json.loads(p)
        print(answer_sheet)
        get_user_code = p_load.get("code")

        tester = Tester.objects.get(code=get_user_code)
        assessor = RiasecAssessor(p_load.get("answers"))
        test_result = assessor.result()

        user = Riasec(code=tester)
        user.result = test_result
        user.R = assessor.r
        user.I = assessor.i
        user.A = assessor.a
        user.S = assessor.s
        user.E = assessor.e
        user.C = assessor.c
        user.save()

        return Response(status=status.HTTP_201_CREATED)


@api_view(["POST"])
def cfit_answer(request):
    score = 0
    answers = request.body
    answers_load = json.loads(answers.decode('utf-8'))
    user_answer = answers_load["answers"]
    f = open('psytest/static/cfitA_answer.json')
    answer_key = json.load(f)["answers"]
    converter = open('psytest/static/ciftA_converter.json')
    converter_load = json.load(converter)
    for i in range(len(user_answer)):
        print(answer_key[i], user_answer[i])
        if user_answer[i]['value'] in answer_key[i]['value']:
            score += 1
    iq = converter_load.get(str(score))
    print(score)
    user_tester = Tester.objects.get(code=answers_load["code"])
    cfit = Cfit(code=user_tester)
    cfit.score = score
    cfit.iq = iq
    cfit.save()

    return Response({"cfit_score": score, "iq": iq},
                    status=status.HTTP_201_CREATED)


@api_view(['POST'])
def spm_answer(request):
    answers = request.body
    answers_load = json.loads(answers.decode('utf-8'))
    user_answer = answers_load["answers"]

    f = open('psytest/static/spm_answer.json')
    converter = open('psytest/static/spm_converter.json')

    final_answer = get_answer(user_answer, f, converter)
    user_tester = Tester.objects.get(code=answers_load["code"]["code"])

    spm = Spm(code=user_tester)
    spm.result = final_answer.get('score')
    spm.iq = final_answer.get('iq')
    spm.save()
    return Response({"spm_score": final_answer.get('score'), "iq": final_answer.get('iq')},
                    status=status.HTTP_201_CREATED)


@api_view(['POST'])
def download_document(request):
    data = request.body
    data_load = json.loads(data.decode('utf-8'))

    report_generator = ReportGenerator(data_load.get('user'), data_load.get('riasec'), data_load.get('spm'),
                                       data_load.get('cfit'))
    report_generator.generate_report()

    print(data_load.get("user").get('name'))
    file_path = data_load.get("user").get('name') + "_" + data_load.get("user").get('code') + ".pdf"
    data = open(file_path, "rb")
    return Response({"data": data.read()}, status=status.HTTP_201_CREATED)


def get_answer(user_answer, answer_key, converter):
    score = 0
    converter_load = json.load(converter)
    answer_key = json.load(answer_key)["answers"]

    for i in range(len(user_answer)):
        if user_answer[i]['value'] == answer_key[i]['value']:
            score += 1

    iq = converter_load.get(str(score))

    return {
        'iq': iq,
        'score': score
    }
