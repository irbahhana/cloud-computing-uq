class RiasecAssessor:

    def __init__(self, data):
        self.data = data
        self.r = 0
        self.i = 0
        self.a = 0
        self.s = 0
        self.e = 0
        self.c = 0
        self.grand_total = 0

    def result(self):
        answer = self.data

        riasec_dict = {
            "r": ["0", "6", "15", "21", "29", "31", "36"],
            "i": ["1", "10", "17", "20", "25", "32", "38"],
            "a": ["2", "7", "16", "22", "26", "30", "40"],
            "s": ["4", "11", "12", "19", "39", "33", "27"],
            "e": ["41", "35", "28", "18", "15", "9", "4"],
            "c": ["37", "34", "24", "23", "14", "8", "7"]
        }

        for key in answer:
            if key['id'] in riasec_dict.get("r"):
                self.r += int(key['value'])
            elif key['id'] in riasec_dict.get("i"):
                self.i += int(key['value'])
            elif key['id'] in riasec_dict.get("a"):
                self.a += int(key['value'])
            elif key['id'] in riasec_dict.get("s"):
                self.s += int(key['value'])
            elif key['id'] in riasec_dict.get("e"):
                self.e += int(key['value'])
            elif key['id'] in riasec_dict.get("c"):
                self.c += int(key['value'])

        self.grand_total = self.r + self.i + self.a + self.s + self.e + self.c
        return self.grand_total

    if __name__ == "__main__":
        result()
