from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Tester)

admin.site.register(Riasec)

admin.site.register(Spm)

admin.site.register(Cfit)
