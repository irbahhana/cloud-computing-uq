from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Tester(models.Model):
    code = models.CharField(max_length=4, primary_key=True, default='')
    name = models.CharField(max_length=30, null=True)


class Riasec(models.Model):
    code = models.OneToOneField(
        Tester,
        on_delete=models.CASCADE,
        primary_key=True
    )
    R = models.IntegerField(null=True)
    I = models.IntegerField(null=True)
    A = models.IntegerField(null=True)
    S = models.IntegerField(null=True)
    E = models.IntegerField(null=True)
    C = models.IntegerField(null=True)
    result = models.IntegerField(null=True)


class Spm(models.Model):
    code = models.OneToOneField(
        Tester,
        on_delete=models.CASCADE,
        primary_key=True
    )
    result = models.IntegerField(null=True)
    iq = models.IntegerField(null=True)


class Cfit(models.Model):
    code = models.OneToOneField(
        Tester,
        on_delete=models.CASCADE,
        primary_key=True
    )
    score = models.IntegerField(null=True)
    iq = models.IntegerField(null=True)
