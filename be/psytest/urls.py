from django.urls import path
from .views import *

urlpatterns = [
    path("index", index, name="testAPI"),
    path("tester/", create_tester, name="register-user"),
    path("tester/<str:code>/", tester, name="detail-user"),
    path("riasec/submit/", riasec_answer, name="riasec_answer"),
    path("spm/submit", spm_answer, name="riasec_answer"),
    path("cfit/submit/", cfit_answer, name="cfit_answer"),
    path("view/<str:code>/", get_user_details, name="get-user"),
    path("result/<str:code>/", get_result, name="get-riasec"),
    path("result/", download_document, name="download-document")
]
