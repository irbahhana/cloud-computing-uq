import json
from reportlab.pdfgen import canvas

class ReportGenerator:

    def __init__(self, user, riasec, spm, cfit):
        self.user = user
        self.riasec = riasec
        self.spm = spm
        self.cfit = cfit
    
    def generate_report(self):
        filename =  self.user.get('name') + "_" + self.user.get('code') + ".pdf"
        title = "Hasil Test minat bakat ananda " + self.user.get('name')
        pdf = canvas.Canvas(filename)
        pdf.setTitle("Hasil test minat bakat " + self.user.get('name'))

        pdf.drawCentredString(300, 770, title)

        text = pdf.beginText(40, 680)

        for k,v in self.riasec.items():
            line = k + ": " + str(v)
            text.textLine(line)
        pdf.drawText(text)
        pdf.save()